from django.urls import path, include
from . import views

app_name = 'main'
urlpatterns = [
    path('', views.home, name='home'),
    path('/advertisement', views.advertisement, name='advertisement'),
    path('/buffer', views.buffer, name='buffer'),
    path('/volume', views.volume, name='volume'),
]