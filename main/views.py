from django.shortcuts import render, redirect
from subprocess import *

# Create your views here.
def home(request):
    return render(request, 'home.html')

def advertisement(request):
    if request.method == 'POST':
        option = int(request.POST['advertisement'])
        if option == 1 :
            run(['ethtool', '-s', 'enp0s3', 'speed', '10', 'duplex', 'full', 'autoneg', 'on'], stdout=PIPE)
        elif option == 2:
            run(['ethtool', '-s', 'enp0s3', 'speed', '100', 'duplex', 'full', 'autoneg', 'on'], stdout=PIPE)
        elif option == 3:
            run(['ethtool', '-s', 'enp0s3', 'speed', '1000', 'duplex', 'full', 'autoneg', 'on'], stdout=PIPE)

    return redirect('main:home')

def buffer(request):
    if request.method == 'POST':
        option = int(request.POST['buffer'])
        if option == 1 :
            run(['ethtool', '-G', 'enp0s3', 'rx', '4096', 'tx', '4096'], stdout=PIPE)
        elif option == 2:
            run(['ethtool', '-G', 'enp0s3', 'rx', '2048', 'tx', '2048'], stdout=PIPE)
        elif option == 3:
            run(['ethtool', '-G', 'enp0s3', 'rx', '1024', 'tx', '1024'], stdout=PIPE)
        elif option == 4:
            run(['ethtool', '-G', 'enp0s3', 'rx', '512', 'tx', '512'], stdout=PIPE)
        elif option == 5:
            run(['ethtool', '-G', 'enp0s3', 'rx', '256', 'tx', '256'], stdout=PIPE)

    return redirect('main:home')

def volume(request):
    if request.method == 'POST':
        option = int(request.POST['volume'])
        master = 'Master'
        if option == 1 :
            run(['amixer', 'set', master, '50%'], stdout=PIPE)
        elif option == 2 :
            run(['amixer', 'set', master, '75%'], stdout=PIPE)
        elif option == 3 :
            run(['amixer', 'set', master, '100%'], stdout=PIPE)

    return redirect('main:home')