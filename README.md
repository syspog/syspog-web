# Port Forwarding Guide
If you haven't setup the Port Forwarding rules for port 8000 in your VM, you can do this following step :

    1. Open VirtualBox
    2. Choose the VM you want to install the service, and then Go to Settings
    3. In Settings page, go to Network > Adapter 1 > Advanced > Port Forwarding
    4. Add New port forwarding rule (green + button on right side)
    5. Insert the new rule with this format :
        - Name : <Your choice>
        - Protocol : TCP
        - Host IP : 127.0.0.1
        - Host Port : <Your Choice> (make sure the port you choose isn't used by other in Host!) e.g 1234
        - Guest IP : 10.0.2.15
        - Guest Port : 8000

# How to Use

First of all, make sure to have Python3, and Django installed. If not, you can install it this way

    sudo apt-get install python3
    sudo apt-get install python3-pip
    pip3 install django

If you already have Python3 and Django installed in your system, you can do this following step :

1. Use the sudo su command

        sudo su

2. Clone the repository on <b>/root</b> directory
    
        git clone https://gitlab.com/syspog/syspog-web 

3. Enter the directory in /root by doing the following command :

        cd /root/syspog-web

4. Copy the manageserver.service file to <b>/lib/systemd/system</b> directory

        cp manageserver.service /lib/systemd/system

5. Activate the service 

        systemctl daemon-reload
        systemctl start manageserver.service
        systemctl enable manageserver.service

Now the webserver is ready to use, after the step 5 is done, you can access the website on localhost, even after the VM restarted

# Launch Server in Host 
To launch the server in host after doing step above (you only to do it once, and the server will be online even after the VM reboot) you can go to you favourite browser and go to the following link :

    localhost:<Your Host Port for Port Forwarding Guest Port 8000>
    example : localhost:1234